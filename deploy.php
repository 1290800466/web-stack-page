<?php

namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'web-stack-page');

// Project repository
set('repository', 'git@bitbucket.org:1290800466/web-stack-page.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys 
set('shared_files', []);
set('shared_dirs', []);

// Writable dirs by web server 
set('writable_dirs', []);

set('keep_releases', 5);


// Hosts
host('47.98.128.149')
    ->user('wangmaolin')
    ->set('branch', 'master')
    ->stage('touping')
    ->set('deploy_path', '/alidata/deploy/web-stack-page');


// Tasks

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
//    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success',
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
